# devolov.gitlab.io

Testing hosting on Gitlab. Modifying to test push capabilities.


`check-diff.sh` checks the publish date of the WordPress page and if it's different from the Gitlab page, it runs `mirrorPage.sh`.

`mirrorPage.sh` pulls that content of the WordPress page using wget, calls `removeTags.py`, moves it to a GIT repo, and pushes it.

`removeTags.py` uses BeautifulSoup to find and remove unwanted divs.

`.gitlab-ci.yml` automatically uploads new commits to master using CI
