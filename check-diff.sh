#!/bin/bash
ORIGINDATE="$(curl -v --silent https://ewbla.wordpress.com 2>&1|grep last-modified|egrep -o [^:]*,[^:]*)"
PUBDATE="$(curl -v --silent https://ewblatest.gitlab.io/ewb-la-test-page/index.html 2>&1|grep last-modified|egrep -o [^:]*,[^:]*)"
printf "ORIGINDATE:  "
echo $ORIGINDATE
printf "PUBDATE:     "
echo $PUBDATE
if [ "$ORIGINDATE" !=  "$PUBDATE" ]
then
  ~/ewb-la-test-page/mirrorPage.sh
fi
