import os, fnmatch
from bs4 import BeautifulSoup as bs

matches = []
for root, dirnames, filenames in os.walk('/home/pi/ewb-la-test-page'):
    for filename in fnmatch.filter(filenames, '*.html'):
        matches.append(os.path.join(root, filename))
print(matches)

for file in (matches):
    print(file)
    f = open(file, 'r+')
    soup = bs(f, 'html.parser')
    elements = soup.find_all('div', ['site-info', 'wpcnt', 'marketing-bar', 'widget widget_eu_cookie_law_widget'])
    for element in elements:
        print(element)
        element.decompose()
    f.close()
    os.remove(file)
    d = open(file, "w")
    d.write(str(soup))
    d.close()
