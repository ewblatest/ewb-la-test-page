cd ~/ewb-la-test-page
wget \
--mirror \
--convert-links  \
--adjust-extension \
--page-requisites  \
--retry-connrefused  \
--exclude-directories=comments \
--execute robots=off \
https://ewbla.wordpress.com
python ./removeTags.py
cp -r -v ~/ewb-la-test-page/ewbla.wordpress.com/. ~/ewb-la-test-page/public
rm -r -v ~/ewb-la-test-page/ewbla.wordpress.com
git add *
git commit -m "Automatic Additions"
git push
